rosservice call /reset
rosservice call /spawn 6 2 0 turtle2
rosservice call /turtle1/set_pen 0 0 0 1 1
rosservice call /turtle1/teleport_absolute 3 2 0
rosservice call /turtle1/set_pen 0 255 160 3 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, 0.8]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2.7, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[-2.7, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, -1.6]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2.7, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rosservice call /turtle2/set_pen 225 110 250 3 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[0.0, 2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[0.0, -0.4, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[0.0, 2.2, 0.0]' '[0.0, 0.0, -3]'
