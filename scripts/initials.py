# Initial imports
from __future__ import print_function
from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi, tau, dist, fabs, cos
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# Initialize moveit_commander with a rospy node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("UR5_initials", anonymous=True)

# Initialize a RobotCommander object
robot = moveit_commander.RobotCommander()

# Initialize a PlanningSceneInterface object
scene = moveit_commander.PlanningSceneInterface()

# Initialize a MoveGroupCommander object
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

# Create a DisplayTrajectory ROS publisher
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# Get the name of the reference frame for this robot:
planning_frame = move_group.get_planning_frame()
print("============ Planning frame: %s" % planning_frame)

# Print the name of the end-effector link for this group:
eef_link = move_group.get_end_effector_link()
print("============ End effector link: %s" % eef_link)

# List of all the groups in the robot:
group_names = robot.get_group_names()
print("============ Available Planning Groups:", robot.get_group_names())

# Print the entire state of the robot:
print("============ Printing robot state")
print(robot.get_current_state())
print("")

# Set joint positions to optimal starting point
# Set desired joint values
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -tau/2
joint_goal[1] = -2
joint_goal[2] = -2
joint_goal[3] = 4
joint_goal[4] = -1.5
joint_goal[5] = -1.5

# Move robot to joint value goals
move_group.go(joint_goal, wait=True)

# Ensure that there is no residual movement
move_group.stop()

# Planning Cartesian path for straight path of K
scale = 1.0
waypoints = []

# Setup desired final pose
wpose = move_group.get_current_pose().pose
wpose.position.z += scale * 0.4  # Move up (z)
waypoints.append(copy.deepcopy(wpose))

# Create plan to reach final pose, with increments of 1 cm
(plan, fraction) = move_group.compute_cartesian_path(
  waypoints, 0.01, 0.0
)

# Move to final position following plan
move_group.execute(plan, wait=True)

# Return to center of straight line of K to draw diagonal lines
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -tau/2
joint_goal[1] = -1.7
joint_goal[2] = -1.8
joint_goal[3] = 3.35
joint_goal[4] = -1.5
joint_goal[5] = -1.5

move_group.go(joint_goal, wait=True)

move_group.stop()

# Draw diagonal upwards path using Cartesian path
waypoints = []

wpose = move_group.get_current_pose().pose
wpose.position.z += scale * 0.2  # Move up (z)
wpose.position.y += scale * 0.2  # Move sideways (y)
waypoints.append(copy.deepcopy(wpose))

(plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0  
)

move_group.execute(plan, wait=True)

# Return to center point to draw downward diagonal
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = -tau/2
joint_goal[1] = -1.7
joint_goal[2] = -1.8
joint_goal[3] = 3.3
joint_goal[4] = -1.5
joint_goal[5] = -1.5

move_group.go(joint_goal, wait=True)

move_group.stop()

# Draw downwards diagonal line using Cartesian path
waypoints = []

wpose = move_group.get_current_pose().pose
wpose.position.z -= scale * 0.2  # Move down (z)
wpose.position.y += scale * 0.2  # Move sideways (y)
waypoints.append(copy.deepcopy(wpose))

(plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0 
)

move_group.execute(plan, wait=True)
