rosservice call /reset
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, 0.8]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2.7, 0.0, 0.0]' '[0.0, 0.0, 0.0]' 
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[-2.7, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, -1.6]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2.7, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
