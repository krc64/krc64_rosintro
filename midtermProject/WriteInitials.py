"""
ME 442/ECE 383: Intro to Robotics
Midterm Project
Purpose: Write initials 'KRC' using the UR5 robot arm with MoveIt

@author: Kristie Chen
With help from the Move Group Python Interface MoveIt tutorial
"""

# Initial imports
from __future__ import print_function
from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi, tau, dist, fabs, cos
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list


# Function move_to takes in 6 different joint angles and moves
# robot to desired angles using joint_goal
# Inputs: move_group - string, name of move group
#         q0, q1, q2, q3, q4, q5 - joint angles for the respective joints
def move_to(move_group, q0, q1, q2, q3, q4, q5):
  joint_goal = move_group.get_current_joint_values()
  joint_goal[0] = q0
  joint_goal[1] = q1
  joint_goal[2] = q2
  joint_goal[3] = q3
  joint_goal[4] = q4
  joint_goal[5] = q5

  move_group.go(joint_goal, wait=True)

  move_group.stop()

# Function cartesian_path takes in 3 values for scaling in x,y,z
# and plans path to desired point using cartesian path
# Inputs: move_group - string, name of move group
#         xscale - list of integers to scale x pose
#         yscale - list of integers to scale y pose
#         zscale - list of integers to scale z pose
def cartesian_path(move_group, xscale, yscale, zscale):
  scale = 1.0
  waypoints = []

  wpose = move_group.get_current_pose().pose

  # Loops through the scale inputs to set waypoints
  for i in range(len(xscale)):
    wpose.position.x += scale * xscale[i]  # Move in (x)
    wpose.position.y += scale * yscale[i]  # Move in (y)
    wpose.position.z += scale * zscale[i]  # Move in (z)
    waypoints.append(copy.deepcopy(wpose))

  (plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0
  )
  
  move_group.execute(plan, wait=True)

# Main function to run initial setup and writing initials
def main():
  # ================= Initial Setup =====================
  # Initialize moveit_commander with a rospy node
  moveit_commander.roscpp_initialize(sys.argv)
  rospy.init_node("UR5_initials", anonymous=True)

  # Initialize a RobotCommander object
  robot = moveit_commander.RobotCommander()

  # Initialize a PlanningSceneInterface object
  scene = moveit_commander.PlanningSceneInterface()

  # Initialize a MoveGroupCommander object
  group_name = "manipulator"
  move_group = moveit_commander.MoveGroupCommander(group_name)

  # Create a DisplayTrajectory ROS publisher
  display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
  )

  # Get the name of the reference frame for this robot:
  planning_frame = move_group.get_planning_frame()
  print("============ Planning frame: %s" % planning_frame)

  # Print the name of the end-effector link for this group:
  eef_link = move_group.get_end_effector_link()
  print("============ End effector link: %s" % eef_link)

  # List of all the groups in the robot:
  group_names = robot.get_group_names()
  print("============ Available Planning Groups:", robot.get_group_names())

  # Print the entire state of the robot:
  print("============ Printing robot state")
  print(robot.get_current_state())
  print("")

  # Move to optimal initial position
  move_to(move_group, 0, -1.14, 2, -0.86, tau/4, 0)

  # ================== Write Letter 'K' ==================
  # Plan Cartesian path for 'K' and move to waypoints
  cartesian_path(move_group, [0, 0, 0, 0, 0], 
    [0, 0, 0.2, -0.2, 0.2], 
    [0.4, -0.2, 0.2, -0.2, -0.2])
 
  # Return to starting point
  move_to(move_group, 0, -1.14, 2, -0.86, 1.55, 0)
  
  # ================== Write Letter 'R' ==================
  # Plan Cartesian path for straight path of 'R' and move there
  cartesian_path(move_group, [0], [0], [0.4])

  # Plan Cartesian path for upper half of curve of 'R' and move there
  cartesian_path(move_group, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    [0.1, 0.025, 0.025, 0.025, 0.025, -0.025, -0.025, -0.025, -0.025, -0.1], 
    [0, -0.004, -0.01, -0.02, -0.066, -0.066, -0.02, -0.01, -0.004, 0])

  # Plan Cartesian path for downward diagonal line of 'R' and move there
  cartesian_path(move_group, [0, 0], [0.05, 0.15], [0, -0.2])

  # Move to starting point for 'C'
  cartesian_path(move_group, [0], [0.1], [0.06])

  # ================== Write Letter 'C' ==================
  # Plan Cartesian path for bottom right quadrant of 'C' and move there
  cartesian_path(move_group, [0, 0, 0, 0, 0, 0], 
    [-0.025, -0.025, -0.025, -0.025, -0.025, -0.025], 
    [-0.025, -0.015, -0.01, -0.005, -0.005, 0])

  # Plan Cartesian path for bottom left quadrant of 'C' and move there
  cartesian_path(move_group, [0, 0, 0, 0, 0, 0, 0, 0], 
    [-0.025, -0.025, -0.025, -0.025, -0.025, -0.025, -0.025, 0], 
    [0, 0.005, 0.005, 0.01, 0.02, 0.05, 0.1, 0.01])
  
  # Plan Cartesian path for upper left quadrant of 'C' and move there
  cartesian_path(move_group, [0, 0, 0, 0, 0, 0, 0, 0], 
    [0, 0.025, 0.025, 0.025, 0.025, 0.025, 0.025, 0.025], 
    [0.01, 0.1, 0.05, 0.02, 0.01, 0.005, 0.005, 0])

  # Plan Cartesian path for upper right quadrant of 'C' and move there
  cartesian_path(move_group, [0, 0, 0, 0, 0, 0], 
    [0.025, 0.025, 0.025, 0.025, 0.025, 0.025], 
    [0, -0.005, -0.005, -0.01, -0.015, -0.025])

if __name__ == "__main__":
  main()
